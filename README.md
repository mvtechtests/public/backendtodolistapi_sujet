Suite à votre candidature de Développeur Back-End, je vous propose de réaliser un test technique pour évaluer le niveau de vos compétences.

Découvrir, installer et tester l'API todoList

Mettre en place un middleware Express permettant la gestion et la remontée des erreurs de façon centralisée

Mettre en place un test unitaire avec les outils que vous trouverez appropriés au contexte, pour cet exercice, tester uniquement la fonctionnalité de mise à jour d'un item de la todoList qui permettra l"utilisation du middleware précédemment crée

Ajouter une nouvelle fonctionnalité de catégorisation des tâches de la todoList

Une tâche possède un statut, nous souhaiterions ajouter à chaque item, une catégorie ( par exemple, "Infra", "Dev", "Doc", "Test" ) Suggestion de fonctionnalités :
ajout d'une nouvelle catégorie
supprimer une catégorie
afficher toutes les catégories
mettre à jour une catégorie
créer une tâche ayant une ou plusieurs catégorie(s)
rechercher toutes les tâches en fonction d'une ou plusieurs catégorie(s)

Quels sont les priorités de l'applicatif que vous suggérez de réaliser pour permettre une pérennité et une sécurité de son utilisation dans le futur ?

## Lancement
`npm install`
`npm run dev`